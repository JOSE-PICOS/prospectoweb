export const environment = {
  production: false,
  apiUrl: 'http://localhost:3000/api',
  dataUrl: 'http://localhost:3000/static'
};
