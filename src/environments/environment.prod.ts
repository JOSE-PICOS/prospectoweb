export const environment = {
  production: true,
  apiUrl: 'https://api-prospectos.jorasystems.com/api',
  dataUrl: 'https://api-prospectos.jorasystems.com/static'
};
