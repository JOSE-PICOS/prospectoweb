import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { Prospecto, ProspectoList } from 'src/app/interfaces/prospectos';
import { SharedModule } from 'src/app/shared/shared.module';
import { DomHandlerService } from 'src/app/dom-handler.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { ProspectosService } from 'src/app/services/prospectos.service';
import { firstValueFrom, lastValueFrom } from 'rxjs';
import { MatPaginatorIntl, PageEvent } from '@angular/material/paginator';
import { ProspectoDialogComponent } from '../components/prospecto-dialog/prospecto-dialog.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-prospectos',
  standalone: true,
  imports: [CommonModule, SharedModule, NgxPaginationModule],
  templateUrl: './prospectos.component.html',
  styleUrl: './prospectos.component.scss'
})
export class ProspectosComponent {

  list: ProspectoList;
  pageSize: number = 12;
  page: number = 0;
  progress: number = 0;
  domHandlerService = inject(DomHandlerService);

  constructor(
    private dialog: MatDialog,
    public _MatPaginatorIntl: MatPaginatorIntl,
    private prospectoService: ProspectosService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ){
    this.getList();
    this._MatPaginatorIntl.itemsPerPageLabel = 'Elementos por pagina:';
  }

  async findById( id: string ){
    return await firstValueFrom(this.prospectoService.fundById(id))
  }

  async openProspectoDialog(id: string, tipo: number){
    const prospecto = id === null ? null : await this.findById(id);
    const listFiles = id === null ? null : await this.getFileList(id);
    
    const dialogRef = this.dialog.open(ProspectoDialogComponent, {
      id: 'dialogProspecto',
      restoreFocus: false,
      data: { prospecto, tipo, listFiles },
      disableClose: true,
    }); 
    dialogRef.afterClosed().subscribe(async dialogResult => {
      console.log( dialogResult );
      if(dialogResult){
        if ( tipo === 1 ){
          this.create(dialogResult.prospecto, dialogResult.files)
        } if (dialogResult.isAutorizado && tipo === 2) {
          this.autorizar(dialogResult.id)
        } if (!dialogResult.isAutorizado && tipo === 2) {
          this.rechazar(dialogResult.id, dialogResult.comentario_rechazo)
        }
      } 
    }); 
  }

  async autorizar(id: string){
    try {
      this.spinner.show('loading');
      const resp = await lastValueFrom(this.prospectoService.autorizar(id));
      if(resp.error) throw new Error();
      this.spinner.hide('loading');
      this.getList();
    } catch (error: any) {
      this.spinner.hide('loading');
      this.toastr.error(error.error.message);
    }
  }

  async rechazar(id: string, comentario_rechazo: string){
    try {
      this.spinner.show('loading');
      const resp = await lastValueFrom(this.prospectoService.rechazar(id, {comentario_rechazo}));
      if(resp.error) throw new Error();
      this.spinner.hide('loading');
      this.getList();
    } catch (error: any) {
      this.spinner.hide('loading');
      this.toastr.error(error.error.message);
    }
  }
  
  async create(prospecto: Prospecto, files: any){
    try {

      this.spinner.show('loading');
      const resp = await lastValueFrom(this.prospectoService.create(prospecto));
     
      if(resp.error) throw new Error();
      this.toastr.success('Se creo un prospecto nuevo...');
      this.prospectoService.uploadFiles(resp.usuario._id, files).subscribe( event => {
       
       //  console.log( event );
        if (event.type === HttpEventType.UploadProgress) {
         //  console.log(Math.round(100 * (event.loaded / event.total)));
          this.progress = Math.round(100 * (event.loaded / event.total));
          if ( Math.round(100 * (event.loaded / event.total)) >= 100 ){
            this.spinner.hide('loading');
          } 
        } else if (event.type === HttpEventType.Response) {
          this.getList();
          this.spinner.hide('loading');
        }
      });
    } catch (error: any) {
      this.spinner.hide('loading');
      this.toastr.error( error.error.message );
    }
  }

  handlePageEvent(event: PageEvent) {
    this.pageSize = event.pageSize;
    this.page = event.pageIndex;
    this.getList();
  }

  async getFileList(id: string){
    return await firstValueFrom(this.prospectoService.getFileList(id));
  }

  async getList(){
    this.list = await firstValueFrom(this.prospectoService.findAll({pageSize: this.pageSize, page: this.page}));
  }

}
