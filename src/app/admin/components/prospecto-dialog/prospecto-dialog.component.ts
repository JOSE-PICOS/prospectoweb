import { Component, Inject } from '@angular/core';
import { UntypedFormGroup, FormGroup, FormControl, Validators, UntypedFormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { ConfirmDialogComponent } from 'src/app/shared/confirm-dialog/confirm-dialog.component';
import { environment } from 'src/environments/environment';
import { RechzarProspectoDialogComponent } from '../rechzar-prospecto-dialog/rechzar-prospecto-dialog.component';

@Component({
  selector: 'app-prospecto-dialog',
  templateUrl: './prospecto-dialog.component.html',
  styleUrl: './prospecto-dialog.component.scss'
})
export class ProspectoDialogComponent {
  public form: UntypedFormGroup;
  formProspecto = new FormGroup({
    _id: new FormControl(null),
    nombre: new FormControl(null, Validators.compose([Validators.required, Validators.minLength(4)])),
    apellido_paterno: new FormControl(null, Validators.compose([Validators.required])),
    apellido_materno: new FormControl(null),
    calle: new FormControl(null, Validators.compose([Validators.required])),
    numero: new FormControl(null, Validators.compose([Validators.required])),
    colonia: new FormControl(null, Validators.compose([Validators.required])),
    codigo_postal: new FormControl(null, Validators.compose([Validators.required])),
    telefono: new FormControl(null, Validators.compose([Validators.required])),
    rfc: new FormControl(null, Validators.compose([Validators.required, Validators.pattern(/^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/)])),
    comentario_rechazo: new FormControl(null),
    status: new FormControl(1)
  });
  files: any = []
  dataUrl = environment.dataUrl;

  constructor(
    public dialogRef2: MatDialogRef<ProspectoDialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: any,
    public fb: UntypedFormBuilder,
    private dialog: MatDialog,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {   
    console.log(this.data)
    if(this.data.prospecto){
      this.formProspecto.patchValue(this.data.prospecto);
    };
    if( this.data.listFiles ){
      this.files = this.data.listFiles;
    }
  }

  onSubmit(){
    if ( this.files.length <= 0){
      this.toastr.warning('No se han proporcionados documentación...')
      return;
    }
    this.dialogRef2.close({prospecto: this.formProspecto.value,files: this.files});
  }

  autorizar(){
    const dialogRefConfirm = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Autorización',
        message: `¿Seguro que quiere autorizar al prospecto: ${this.formProspecto.value.nombre} ${this.formProspecto.value.apellido_paterno} ${this.formProspecto.value.apellido_materno}?`
      },
      id: 'confirm-dialog',
      restoreFocus: false,
      maxWidth: "400px",
      role: 'dialog'
    }); 
  
    dialogRefConfirm.afterClosed().subscribe(async dialogResult => { 
      if(dialogResult){
        this.dialogRef2.close({id: this.formProspecto.value._id, isAutorizado: true});
      }
  
    }); 
  }

  rechazar(){
    const dialogRefReject = this.dialog.open(RechzarProspectoDialogComponent, {
      data: {
        title: 'Autorización',
        message: `¿Seguro que quiere autorizar al prospecto: ${this.formProspecto.value.nombre} ${this.formProspecto.value.apellido_paterno} ${this.formProspecto.value.apellido_materno}?`
      },
      id: 'reject-dialog',
      restoreFocus: false,
      maxWidth: "400px",
    }); 
  
    dialogRefReject.afterClosed().subscribe(async dialogResult => { 
      if(dialogResult){
         this.dialogRef2.close({id: this.formProspecto.value._id, comentario_rechazo: dialogResult, isAutorizado: false})
      }
  
    }); 
  }

  async onFileSelect(event: any){
    for (const item of event.files) {
      this.files.push({file: item})
    }
  }

  formatBytes(bytes: number): string {
    const UNITS = ['Bytes', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const factor = 1024;
    let index = 0;

    while (bytes >= factor) {
      bytes /= factor;
      index++;
    }

    return `${parseFloat(bytes.toFixed(2))} ${UNITS[index]}`;
  }

  deleteFile(index: number){
    this.files.splice(index,1)
  }

  async dowloadArchivo( archivo: string ){
    const url = `${this.dataUrl}/${this.formProspecto.value._id}/${archivo}`
    const a = document.createElement("a");
    const blob = await fetch(url).then( res => res.blob() );
    a.href = await URL.createObjectURL(blob);
    a.download = archivo;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }

  close(){
    if ( !this.formProspecto.value._id ){
      const dialogRefConfirm = this.dialog.open(ConfirmDialogComponent, {
        data: {
          title: 'Salir',
          message: `Al salir se perdera cualquier dato ingresado, ¿seguro que desea salir?`
        },
        id: 'confirm-dialog',
        restoreFocus: false,
        maxWidth: "400px",
        role: 'dialog'
      }); 
    
      dialogRefConfirm.afterClosed().subscribe(async dialogResult => { 
        if(dialogResult){
          this.dialogRef2.close();
        }
    
      }); 
    } else {
      this.dialogRef2.close();
    }
  }
}
