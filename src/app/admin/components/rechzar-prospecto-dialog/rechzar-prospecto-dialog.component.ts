import { Component, Inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-rechzar-prospecto-dialog',
  standalone: true,
  imports: [CommonModule, SharedModule, FormsModule, ReactiveFormsModule],
  templateUrl: './rechzar-prospecto-dialog.component.html',
  styleUrl: './rechzar-prospecto-dialog.component.scss'
})
export class RechzarProspectoDialogComponent {

  comentario_rechazo: string = '';

  constructor(public dialogRef: MatDialogRef<RechzarProspectoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
  }
  
  onConfirm(): void {
    if ( this.comentario_rechazo === '' ){
      this.toastr.warning('Debe de proporcionar un comentario antes de rechazar a un prospecto...')
      return;
    }
    this.dialogRef.close(this.comentario_rechazo);
  }
  
  onDismiss(): void {
  this.dialogRef.close(false);
  }
}
