import { HttpClient, HttpXhrBackend } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

export class ModelsService {
  private http: HttpClient
  private _modelo!: string

  set modelo(value: any) { this._modelo = value}

  get apiUrl() { return environment.apiUrl}
  get headers() {
    return {
      headers: {
        'Content-Type': 'application/json',
      }
    }
  }

  get headersFormData() {
    return {
      headers: {
        'enctype': 'multipart/formdata',
        'Cache-Control': 'no-cache'
      }
    }
  }

  get url() { return `${ this.apiUrl }/${this._modelo}` }
  
  constructor(
    modelo: string
  ) { 
    this._modelo = modelo
    this.http =  new HttpClient(new HttpXhrBackend({
        build: () => new XMLHttpRequest()
    }));
  }

  findAll<T>(params?: any): Observable<T>{
    return this.http.get<T>( this.url, {...this.headers, params} )
  }

  find<T>(id: any, params?: any): Observable<T>{
    return this.http.get<T>( `${this.url}/${id}`, {...this.headers, params} )
  }

  put<T>(endpoint: string, id: any, params?: any): Observable<T>{
    return this.http.put<T>( `${this.url}/${endpoint}/${id}`, {...this.headers, params} )
  }

  get<T>( endpoint: string, params?: any): Observable<T>{
    return this.http.get<T>( `${this.url}/${endpoint}`, {...this.headers, params} )
  }

  create<T>(body: any): Observable<T>{
    return this.http.post<T>( this.url, body, this.headers )
  }

  post<T>(endpoint: string, body: any): Observable<T>{
    return this.http.post<T>( `${this.url}/${endpoint}`, body, this.headers)
  }

  patch<T>(endpoint: any, body?: any): Observable<T>{
    return this.http.patch<T>( `${this.url}/${endpoint}`, body, this.headers)
  }

  postFormData<T>(endpoint: string, body: any): Observable<any>{
    return this.http.post<any>( `${this.url}/${endpoint}`, body,{ ...this.headersFormData, observe: 'events', reportProgress: true },  )
  }
  
}
