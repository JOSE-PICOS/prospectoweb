import { Injectable } from '@angular/core';
import { ModelsService } from './models.service';
import { Prospecto, ProspectoList } from '../interfaces/prospectos';
import { Observable, map, catchError, of } from 'rxjs';
import { SlicePipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ProspectosService {

  prospectoModel: ModelsService;
  uploadModel: ModelsService;
  constructor() { 
    this.prospectoModel = new ModelsService('prospectos');
    this.uploadModel = new ModelsService('upload');
  }

  findAll(params?: any){
    return this.prospectoModel.findAll<ProspectoList>(params);
  }

  fundById( id: string ){
    return this.prospectoModel.find<ProspectoList>(id);
  }

  create(body: Prospecto){
    return this.prospectoModel.create<any>(body);
  }

  uploadFiles( id: string, files: any ): Observable<any>{
    console.log( files );
    const fd = new FormData()
    for (const item of files) {
      fd.append('archivo', item.file);
    }
    return this.uploadModel.postFormData<any>(`${id}`,fd).pipe( map( (event) => {
      return event;
    }),
    catchError((error) => {
       return of({ isError: true,  error });
     }) );
  }

  getFileList(id: string){
    return this.uploadModel.find<string[]>(id);
  }

  autorizar(id: string){
    return this.prospectoModel.patch<any>(`autorizar-prospecto/${id}`);
  }

  rechazar(id: string, body: any){
    return this.prospectoModel.patch<any>(`rechazar-prospecto/${id}`, body);
  }

}
