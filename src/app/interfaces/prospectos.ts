
export interface Prospecto {
    _id?: string;
    nombre: string;
    apellido_paterno: string;
    apellido_materno: string;
    calle: string;
    numero: string;
    colonia: string;
    codigo_postal: number;
    telefono: string;
    rfc: string;
    status?: number;
}

export interface ProspectoList {
    prospectos: Prospecto[];
    page: number;
    pageSize: number;
    total: number;
}

